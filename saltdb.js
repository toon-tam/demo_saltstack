
const request = require('request');
const dotenv = require('dotenv');
const EventEmitter = require('node:events');
const salt_stack = require('./database/salt_stack.js');
const { log } = require('node:console');

module.exports = class saltdb {
    constructor(salt, client) {
        this.salt = salt;
        this.client = client;
        this.salt_stack = new salt_stack(this.client);
        this.emitter = new EventEmitter();
        this.on = this.emitter.on;
        this.removeAllListeners = this.emitter.removeAllListeners;
        this.emit = this.emitter.emit;
        this.minions = []
    }
    async get_date() {
        let d = new Date();
        let year = d.getFullYear();
        let month = d.getMonth() + 1;
        let day = d.getDate()
        let hours = d.getHours() + 7
        let min = d.getMinutes();
        let sec = d.getSeconds()
        let time = year + "-" + month + "-" + day + " " + hours + ":" + min + ":" + sec
        // console.log(time);
        return time;
    }

    async recheck() {
        const value = this.salt.login().then(async () => {
            this.salt.fun("*", "test.ping").then(async data => {
                console.log(data);
                return data
            })
            return value
        })
    }

    async line_notify(minion_id) {
        for (let index = 0; index < 5; index++) {
                if (index == 4) {
                    dotenv.config();
                    const url_line_notification = "https://notify-api.line.me/api/notify";
                    request({
                        method: 'POST',
                        uri: url_line_notification,
                        header: {
                            'Content-Type': 'multipart/form-data',
                        },
                        auth: {
                            bearer: process.env.TOKEN,
                        },
                        form: {
                            message: minion_id + ' is Offline'
                        },
                    }, (err, httpResponse, body) => {
                        // console.log(httpResponse);
                        if (err) {
                            console.log(err)
                        } else {
                            console.log(body)
                        }
                    });
                }
        }
    }

    async update_minion(minion_id, time) {
        this.on('updateMinion', (user_id, time) => {
            this.salt_stack.insert_userinfo_db(user_id, 'Online', time).then(data => {
                console.log("insert user results===>", data);
            }).catch(async () => {
                this.salt_stack.update_status_user(user_id, 'Online', time);
                console.log('update status user');
            });
        })
        this.emit('updateMinion', minion_id, time)
        this.removeAllListeners('updateMinion');
        // await this.check_minion()

    }

    async check_status_minion(minion){
        var all_minion = await this.get_minion()
        var minions = all_minion.rows
        // console.log(minions);
        // console.log(minion);
        // this.minions.push(minion)
        this.on('updateOffline', async (user_id, time) => {
                this.salt_stack.insert_userinfo_db(user_id, 'Offline', time).then(data => {
                    console.log("update user offline===>", data);
                }).catch(async () => {
                    this.salt_stack.update_status_user(user_id, 'Offline', time)
                    console.log('update user offline');
                })
        })
        
        if (this.minions.length == 0) {
            this.minions.push(minion)
        }else if (this.minions.length > 0) {
            var s_minion = this.minions.includes(minion)
            if (s_minion == false) {
                this.minions.push(minion)
            }else if (s_minion == true) {
                for (let index = 0; index < all_minion.length; index++) {
                    const element = all_minion[index];
                    const minion_id = element.minion_id
                    const off_minion = this.minions.includes(minion_id)
                    // console.log(minion_id);
                    // console.log(off_minion);
                    if (off_minion == false) {
                        // console.log(element);
                        const isoDate = element.create_time
                        const date = new Date(isoDate);
                        const year = date.getFullYear();
                        const month = String(date.getMonth() + 1).padStart(2, '0');
                        const day = String(date.getDate()).padStart(2, '0');
                        const hours = String(date.getHours()).padStart(2, '0');
                        const minutes = String(date.getMinutes()).padStart(2, '0');
                        const seconds = String(date.getSeconds()).padStart(2, '0');
                        const formattedDate = `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
                        this.emit('updateOffline',minion_id,formattedDate)
                        this.removeAllListeners('updateOffline')
                        this.minions = []
                    }
                }
            }
        }
        console.log(this.minions);
    }



    async get_minion() {
        var get_status_minion = await this.salt_stack.get_user()
        var all_minion = get_status_minion.rows
        return all_minion
    }

    async get_event() {
        //////////////////// event \\\\\\\\\\\\\\\\\\\\\\\\
        this.salt.login().then(async () => {
            const events = await this.salt.eventSource()
            events.onopen = () => {
                console.log("Connected to Salt Master Event Bus")
            }
            events.onmessage = async (data) => {
                // console.log("Got event from Salt Master Events Bus", data)
                data = JSON.parse(data.data).data
                console.log(data);
                

                if (data.diskusage != undefined) {
                    var diskusage = data.diskusage
                    var time = data._stamp.split('T')[0] + " " + data._stamp.split('T')[1]
                    // console.log(time);
                    var minion_id = data.id
                    await this.check_status_minion(minion_id)
                    this.on('insertDisk', (user_id, diskusage, time) => {
                        this.salt_stack.insert_disk_db(user_id, diskusage, time).then(data => {
                            console.log("insert diskusage results");
                        }).catch(async () => {
                            this.salt_stack.update_diskusage(user_id, diskusage, time)
                            console.log('update disk');
                        });
                        this.salt_stack.insert_log_disk(user_id, diskusage, time)
                    })


                    this.emit('insertDisk', minion_id, diskusage, time)
                    this.removeAllListeners('insertDisk');
                    await this.update_minion(minion_id, time)

                    // await this.check_minion(minion_id)
                    



                } else if (data.memusage != undefined) {
                    var memusage = data.memusage
                    var time = data._stamp.split('T')[0] + " " + data._stamp.split('T')[1]
                    var user_id = data.id

                    await this.salt_stack.insert_mem_db(user_id, memusage, time).then(data => {
                        console.log("insert memusage results");
                    }).catch(async () => {
                        await this.salt_stack.update_memusage(user_id, memusage, time)
                        console.log('update memory');
                    });

                    await this.salt_stack.insert_log_mem(user_id, memusage, time)

                } else if (data.data != undefined && data.data != 'ping' && data.data.cpustats != undefined) {
                    // console.log("data===>",data);
                    var all = data.data
                    var cpustats = all.cpustats

                    var time = data._stamp.split('T')[0] + " " + data._stamp.split('T')[1].split('.')[0]
                    var user_id = data.id
                    if (cpustats.cpu != undefined) {
                        var cpu = cpustats.cpu
                        const idle = cpu.idle
                        const softirq = cpu.softirq
                        const system_use = cpu.system
                        const user_use = cpu.user
                        const total_cpu = idle + system_use + user_use
                        const cpu_used = 1 - (idle / total_cpu)
                        const persent_cpu_used = cpu_used * 100

                        const meminfo = all.meminfo
                        const memTotal = meminfo.MemTotal.value
                        const cached = meminfo.Cached.value
                        const memAvailable = meminfo.MemAvailable.value
                        const buffers = meminfo.Buffers.value
                        // console.log(create_time);

                        // var results = await this.insert_meminfo_db(user_id, memTotal, memAvailable, buffers, cached, create_time).then(data => {
                        //     console.log("insert memusage results===>", data);
                        // }).catch(async () => {
                        //     var update_meminfo = await this.update_meminfo(user_id, memTotal, memAvailable, buffers, cached, create_time)
                        //     console.log('update meminfo', update_meminfo);
                        // });

                        await this.salt_stack.insert_cpu_db(user_id, idle, persent_cpu_used, time).then(data => {
                            console.log("insert cpu results");
                        }).catch(async () => {
                            await this.salt_stack.update_cpu(user_id, idle, persent_cpu_used, time)
                            console.log('update cpu');
                        });
                        await this.salt_stack.insert_log_cpu(user_id, idle, persent_cpu_used, time)
                    }
                } else if (Object.keys(data)[0] == '1m') {
                    console.log(data.id);
                    const one_min = Object.values(data)[0]
                    const five_min = Object.values(data)[1]
                    const fifteen_min = Object.values(data)[2]
                    var user_load = data.id
                    var time = data._stamp.split('T')[0] + " " + data._stamp.split('T')[1]
                    await this.salt_stack.insert_load_db(user_load, five_min, time).then(data => {
                        console.log("insert load results===>", data);
                    }).catch(async () => {
                        await this.salt_stack.update_load(user_load, five_min, time)
                        console.log("insert load results===>", data);
                    });
                    await this.salt_stack.insert_load_avg(user_load, one_min, five_min, fifteen_min, time)
                } else if (data.data == 'ping') {
                    var all_minion = this.get_minion()
                    console.log(all_minion);
                    this.on('insertOnline', async (element, time) => {
                        // console.log(element);
                        this.salt_stack.insert_userinfo_db(element, 'Online', time).then(data => {
                            console.log("insert user results===>", data);
                        }).catch(async () => {
                            this.salt_stack.update_status_user(element, 'Online', time);
                            console.log('update status user');
                        });
                    })

                    this.on('insertOffline', async (user_id, status, time) => {
                        if (status == "Offline") {
                            console.log('test');

                        } else if (status == "Online") {
                            this.salt_stack.insert_userinfo_db(user_id, 'Offline', time).then(data => {
                                console.log("insert user results===>", data);
                            }).catch(async () => {
                                this.salt_stack.update_status_user(user_id, 'Offline', time)
                                console.log('update status user');
                            })
                        }
                    })

                    if (all_minion.length == 0) {
                        // console.log('test');
                        this.emit('insertOnline', data.id, data._stamp)
                        this.removeAllListeners('insertOnline');
                    } else if (all_minion.length != 0) {
                        for (let index = 0; index < all_minion.length; index++) {
                            // console.log(all_user);
                            const element = all_minion[index];
                            const minion_id = element.minion_id
                            const status = element.status
                            console.log(minion_id);
                        }
                        var time = data._stamp.split('T')[0] + " " + data._stamp.split('T')[1]
                        // const create_time = element.create_time

                        // if (data.id == user_id) {                                
                        this.emit('insertOnline', data.id, time)
                        this.removeAllListeners('insertOnline');
                        // } else if (data.id != user_id) {
                        // console.log(user_id);
                        // this.emit('insertOnline', data.id, create_time)
                        // this.removeAllListeners('insertOnline');
                        //     this.emit('insertOffline', user_id, status)
                        //     this.removeAllListeners('insertOffline');
                        // }
                        // }
                    }
                } else if (data.tag == 'minion_start') {
                    // console.log('data.tag');
                    var time = data._stamp.split('T')[0] + " " + data._stamp.split('T')[1]
                    this.on('insertNew', async (user_id, time) => {
                        this.salt_stack.insert_userinfo_db(user_id, 'Online', time).then(data => {
                            console.log("insert new user results===>", data);
                        }).catch(async () => {
                            this.salt_stack.update_status_user(user_id, 'Online', time)
                            console.log('update new status user');
                        })
                    })


                    this.emit('insertNew', data.id, time)
                    this.removeAllListeners('insertNew');
                    // this.emit('insertOnline', data.id, data._stamp)
                    // this.removeAllListeners('insertOnline');
                } else if (data.pub) {
                    console.log('Salt minion will wait for 10 seconds before attempting to re-authenticate');
                    var time = data._stamp.split('T')[0] + " " + data._stamp.split('T')[1]
                    this.on('insertNew', async (user_id) => {
                        this.salt_stack.insert_userinfo_db(user_id, 'NotConnect', time).then(data => {
                            console.log("insert new user results===>", data);
                        }).catch(async () => {
                            this.salt_stack.update_status_user(user_id, 'Online', time)
                            console.log('update new status user');
                        })
                    })


                    this.emit('insertNew', data.id)
                    this.removeAllListeners('insertNew');
                } else if (data.minions) {
                    // console.log('minion===>', data.minions);
                    this.on('insertMinion', async (user_id, time) => {
                        this.salt_stack.insert_userinfo_db(user_id, 'NotConnect', time).then(data => {
                            console.log("insert new user results===>", data);
                        }).catch(async () => {
                            // this.salt_stack.update_status_user(user_id, 'Offline', await this.get_date())
                            console.log('update new status user');
                        })
                    })
                    for (let index = 0; index < data.minions.length; index++) {
                        const element = data.minions[index];
                        // console.log(element);
                        const time = data._stamp.split('T')[0] + " " + data._stamp.split('T')[1]


                        this.emit('insertMinion', element, time)
                        this.removeAllListeners('insertMinion');

                    }
                } else {
                    console.log("Got event from Salt Master Events Bus", data)
                    return data
                }

            }

            events.onerror = async (err) => {
                console.log("Got event from Salt Master Events err", err)
                this.on('error', () => {
                    // console.log(element);
                    this.get_event()
                })
                this.emit("error")
                if (err?.status === 401 || err?.status === 403) {
                    console.log("Not authorized")
                    this.emit("error")
                    this.removeAllListeners('error');
                };
                return err
            }

        })

        //////////////////// event \\\\\\\\\\\\\\\\\\\\\\\\
    }

}


