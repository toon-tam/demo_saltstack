
const request = require('request');
const dotenv = require('dotenv');
const EventEmitter = require('node:events');
const salt_stack = require('./database/salt_stack.js');
const { log } = require('node:console');
module.exports = class saltdb {
    constructor(salt, client) {
        this.salt = salt;
        this.client = client;
        this.salt_stack = new salt_stack(this.client);
        this.emitter = new EventEmitter();
        this.on = this.emitter.on;
        this.removeAllListeners = this.emitter.removeAllListeners;
        this.emit = this.emitter.emit;
    }
    async get_date() {
        let d = new Date();
        let year = d.getUTCFullYear();
        let month = d.getUTCMonth() + 1;
        let day = d.getUTCDate()
        let hours = d.getUTCHours()
        let min = d.getUTCMinutes();
        let sec = d.getUTCSeconds()
        let time = year + "-" + month + "-" + day + " " + hours + ":" + min + ":" + sec
        return time;
    }
    get_user() {
        this.on('event', function (validationResult) {
            console.log('validation result is ' + validationResult);
        })
    }

    async get_all_status() {
        const all_log = await this.salt.login().then(async () => {
            const salt_all = await this.salt.fun("*", "test.ping").then(async data => {
                console.log(data);
                var user_id = Object.keys(data.return[0])
                console.log(user_id);
                this.on('insertOnline', async (element) => {
                    // console.log(element);
                    await this.salt_stack.insert_userinfo_db(element, 'Online', await this.get_date()).then(data => {
                        console.log("insert user results===>", data);
                    }).catch(async () => {
                        await this.salt_stack.update_status_user(element, 'Online', await this.get_date());
                        console.log('update status user');
                    });


                })
                this.on('insertOffline', async (element) => {
                    console.log(element);
                    await this.salt_stack.insert_userinfo_db(element, 'Offline', await this.get_date()).then(data => {
                        console.log("insert user results===>", data);
                    }).catch(async () => {
                        var get_status_user = await this.salt_stack.get_user()
                        var all_user = get_status_user.rows
                        // console.log(all_user);
                        for (let index = 0; index < all_user.length; index++) {
                            const element_user = all_user[index];
                            const get_user_id = element_user.user_id
                            const get_status = element_user.status
                            if (get_user_id == element) {
                                if (get_status == "Online") {
                                    await this.salt_stack.update_status_user(element, 'Offline', await this.get_date())
                                    console.log('update status user');
                                } else if (get_status == "Offline") {
                                    for (let index = 0; index < 5; index++) {
                                        console.log(index);
                                        var recheck = await this.recheck_status(element)
                                        console.log(Object.values(recheck.return[0])[0]);
                                        if (Object.values(recheck.return[0])[0] == false && index == 4) {
                                            dotenv.config();
                                            const url_line_notification = "https://notify-api.line.me/api/notify";
                                            request({
                                                method: 'POST',
                                                uri: url_line_notification,
                                                header: {
                                                    'Content-Type': 'multipart/form-data',
                                                },
                                                auth: {
                                                    bearer: process.env.TOKEN,
                                                },
                                                form: {
                                                    message: element + ' is Offline'
                                                },
                                            }, (err, httpResponse, body) => {
                                                console.log(httpResponse);
                                                if (err) {
                                                    console.log(err)
                                                } else {
                                                    console.log(body)
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        }
                    });

                })

                for (let index = 0; index < user_id.length; index++) {
                    const element = user_id[index];
                    const status = data.return[0][element]

                    if (status == true) {
                        this.emit('insertOnline', element)
                        // this.removeAllListeners('insertOnline');
                        // this.get_event();
                    } else if (status == false) {

                        // console.log(element);
                        this.emit('insertOffline', element)
                        // this.removeAllListeners('insertOffline');

                    }
                }

            })

            // return salt_all
        })
        // return all_log
        // this.get_event();
    }
}


