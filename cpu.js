const resourceUsage = async function(containerName) {
    try {
        let that = this;
        var info;
        if (containerName) {
            info = await new Promise(function (resolve2, reject2) {
                that.docker.getContainer(containerName).stats({ stream: false }, (err, data) => {
                    if (err) {
                        reject2(err);
                        return;
                    }
                    // console.log(JSON.stringify(data))
                    function calculateCPUPercentUnix(previousCPU, previousSystem, v) {
                        var cpuPercent = null
                        // calculate the change for the cpu usage of the container in between readings
                        var cpuDelta = parseFloat(v.cpu_stats.cpu_usage.total_usage) - parseFloat(previousCPU)
                        // calculate the change for the entire system between readings
                        var systemDelta = parseFloat(v.cpu_stats.system_cpu_usage) - parseFloat(previousSystem)
                        // console.log(cpuDelta,systemDelta)
                        // Check if percpu_usage is available

                        if (v?.cpu_stats?.cpu_usage?.percpu_usage) {
                            const numCPUs = v.cpu_stats.cpu_usage.percpu_usage.length;
                            if (systemDelta > 0 && cpuDelta > 0) {
                                // cpuPercent = (cpuDelta / systemDelta) * parseFloat((v.cpu_stats.cpu_usage.percpu_usage).length) * 100.0
                                cpuPercent = (cpuDelta / systemDelta) * numCPUs * 100;
                                cpuPercent = cpuPercent.toFixed(2)
                            }
                        }
                        return cpuPercent
                    }
                    function calculateMemPercentUnix(usageMem, limitMem) {

                        return (usageMem / limitMem * 100).toFixed(2)
                    }
                    var v = data
                    var previousCPU = v.precpu_stats.cpu_usage.total_usage
                    var previousSystem = v.precpu_stats.system_cpu_usage
                    var usageMem = v.memory_stats.usage
                    var limitMem = v.memory_stats.limit
                    var cpu = calculateCPUPercentUnix(previousCPU, previousSystem, v)
                    var memory = calculateMemPercentUnix(usageMem, limitMem)
                    // resolve2(data)
                    resolve2(
                        {
                            "name": data.name.split('/')[1],
                            "memory": parseFloat(memory) || 0,
                            "cpu": parseFloat(cpu)
                        }
                    )
                })
            })

        }
        else {
            throw new error('please spicify containerName');
        }
        return {
            statusCode: 200,
            name: containerName,
            info: info
        };
    } catch (error) {
        return {
            error: error,
            statusCode: 400

        }
    }
}


resourceUsage();
