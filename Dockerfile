FROM node:latest

#Create app directory
WORKDIR /app

#Install app dependencies
COPY . /app

# RUN apt-get update && \
#     apt-get install -y curl && \
#     # curl -fsSL https://deb.nodesource.com/setup_14.x | bash - && \
#     apt-get install -y nodejs && \
#     apt-get clean && \
#     rm -rf /var/lib/apt/lists/*

RUN npm install

#Bundle app source
# COPY . .

# EXPOSE 5050 8080

# CMD ["npm","start"]
CMD [ "node", "index.js" ]

# RUN node index.js