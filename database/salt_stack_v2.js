module.exports = class salt_stack_v2 {
    constructor(client) {
        this.client = client;
    }

    async insert_userinfo_db(uuid,minion_name,status,created_time,updated_time) {
        const sql = `INSERT INTO vm_profile(uuid,name,status,created_time,updated_time) VALUES('${uuid}','${minion_name}','${status}','${created_time}','${updated_time}')`
        const result = await this.client.query(sql);
        // console.log("result--->", result);
        return result
    }

    async get_user() {
        const sql = 'SELECT * FROM vm_profile'
        const result = await this.client.query(sql)
        return result
    }


    async update_status_user(uuid, status, updated_time) {
        const sql = `UPDATE vm_profile SET updated_time = '${updated_time}', status = '${status}' WHERE uuid = '${uuid}'`
        const result = await this.client.query(sql)
        return result
    }

    async update_cpu_user(uuid, status,cpu_total, updated_time) {
        const sql = `UPDATE vm_profile SET updated_time = '${updated_time}', status = '${status}', cpu = ${cpu_total} WHERE uuid = '${uuid}'`
        const result = await this.client.query(sql)
        return result
    }

    async update_mem_user(uuid, status, mem_total, updated_time) {
        const sql = `UPDATE vm_profile SET updated_time = '${updated_time}', status = '${status}', mem = ${mem_total} WHERE uuid = '${uuid}'`
        const result = await this.client.query(sql)
        return result
    }


    async insert_logs_disk(user_id, diskusage, create_time) {
        // console.log(uptime[0]);
        const sql = `INSERT INTO logs_disk(uuid,disk_used,create_time) VALUES('${user_id}',${diskusage},'${create_time}')`
        const result = await this.client.query(sql);
        return result
    }

    async insert_logs_mem(user_id, memusage, create_time) {
        // console.log(mem[0]);
        const sql = `INSERT INTO logs_mem(uuid,mem_used,create_time) VALUES('${user_id}',${memusage},'${create_time}')`
        const result = await this.client.query(sql);
        return result
    }

    async insert_logs_cpu(user_id,persent_cpu_used, create_time) {
        // console.log(mem[0]);
        const sql = `INSERT INTO logs_cpu(uuid,cpu_used,create_time) VALUES('${user_id}',${persent_cpu_used},'${create_time}')`
        const result = await this.client.query(sql);
        return result
    }

    async insert_logs_load(user_id,one_min, five_min, fifteen_min, create_time) {
        // console.log(mem[0]);
        const sql = `INSERT INTO logs_load(uuid,one_min, five_min, fifteen_min,create_time) VALUES('${user_id}',${one_min},${five_min},${fifteen_min},'${create_time}')`
        const result = await this.client.query(sql);
        return result
    }
}