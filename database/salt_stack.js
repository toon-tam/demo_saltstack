module.exports = class salt_stack {
    constructor(client) {
        this.client = client;
    }

    async insert_userinfo_db(uname, status, time) {
        const sql = `INSERT INTO user_info(minion_id,status,create_time) VALUES('${uname}','${status}','${time}')`
        const result = await this.client.query(sql);
        // console.log("result--->", result);
        return result
    }

    async get_user() {
        const sql = 'SELECT * FROM user_info'
        const result = await this.client.query(sql)
        return result
    }

    async get_disk() {
        const sql = 'SELECT * FROM disk'
        const result = await this.client.query(sql)
        return result
    }

    async update_meminfo(user_id, memTotal, memAvailable, buffers, cached, create_time) {
        const sql = `UPDATE mem_info SET create_time = '${create_time}', mem_total = ${memTotal}, mem_available = ${memAvailable}, buffers = ${buffers}, cached = ${cached} WHERE user_id = '${user_id}'`
        const result = await this.client.query(sql)
        return result
    }

    async update_cpu(user_id, idle,persent_cpu_used, create_time) {
        const sql = `UPDATE cpu SET create_time = '${create_time}', idle = ${idle}, cpu_used =${persent_cpu_used} WHERE minion_id = '${user_id}'`
        const result = await this.client.query(sql)
        return result
    }

    async update_memusage(user_id, memusage, create_time) {
        const sql = `UPDATE memory SET create_time = '${create_time}', memusage = ${memusage} WHERE minion_id = '${user_id}'`
        const result = await this.client.query(sql)
        return result
    }
    async update_load(user_id, five_min, create_time) {
        const sql = `UPDATE load_avg SET create_time = '${create_time}', five_min = ${five_min} WHERE minion_id = '${user_id}'`
        const result = await this.client.query(sql)
        return result
    }

    async update_diskusage(user_id, diskusage, create_time) {
        const sql = `UPDATE disk SET create_time = '${create_time}', diskusage = ${diskusage} WHERE minion_id = '${user_id}'`
        const result = await this.client.query(sql)
        return result
    }

    async update_status_user(user_id, status, create_time) {
        const sql = `UPDATE user_info SET create_time = '${create_time}', status = '${status}' WHERE minion_id = '${user_id}'`
        const result = await this.client.query(sql)
        return result
    }

    async insert_disk_db(user_id, diskusage, create_time) {
        // console.log(uptime[0]);
        const sql = `INSERT INTO disk(minion_id,diskusage,create_time) VALUES('${user_id}',${diskusage},'${create_time}')`
        const result = await this.client.query(sql);
        return result
    }

    async insert_mem_db(user_id, memusage, create_time) {
        // console.log(mem[0]);
        const sql = `INSERT INTO memory(minion_id,memusage,create_time) VALUES('${user_id}',${memusage},'${create_time}')`
        const result = await this.client.query(sql);
        return result
    }

    async insert_cpu_db(user_id, idle,persent_cpu_used, create_time) {
        // console.log(cpu[0]);
        const sql = `INSERT INTO cpu(minion_id,idle,cpu_used,create_time) VALUES('${user_id}',${idle},${persent_cpu_used},'${create_time}')`
        const result = await this.client.query(sql);
        return result
    }
    async insert_load_db(user_id, five_min, create_time) {
        // console.log(cpu[0]);
        const sql = `INSERT INTO load_avg(minion_id,five_min,create_time) VALUES('${user_id}',${five_min},'${create_time}')`
        const result = await this.client.query(sql);
        return result
    }

    // async insert_meminfo_db(user_id, memTotal, memAvailable, buffers, cached, create_time) {
    //     // console.log(mem[0]);
    //     const sql = `INSERT INTO mem_info(user_id,mem_total,mem_available,buffers,cached,create_time) VALUES('${user_id}',${memTotal},${memAvailable},${buffers},${cached},'${create_time}')`
    //     const result = await this.client.query(sql);
    //     return result
    // }

    async insert_load_avg(user_id,one_min, five_min,fifteen_min, create_time) {
        // console.log(mem[0]);
        const sql = `INSERT INTO logs_load(minion_id,one_min,five_min,fifteen_min,create_time) VALUES('${user_id}',${one_min},${five_min},${fifteen_min},'${create_time}')`
        const result = await this.client.query(sql);
        return result
    }

    async insert_log_disk(user_id, diskusage, create_time) {
        // console.log(uptime[0]);
        const sql = `INSERT INTO logs_disk(minion_id,disk_used,create_time) VALUES('${user_id}',${diskusage},'${create_time}')`
        const result = await this.client.query(sql);
        return result
    }

    async insert_log_mem(user_id, memusage, create_time) {
        // console.log(mem[0]);
        const sql = `INSERT INTO logs_mem(minion_id,mem_used,create_time) VALUES('${user_id}',${memusage},'${create_time}')`
        const result = await this.client.query(sql);
        return result
    }

    async insert_log_cpu(user_id, idle,persent_cpu_used, create_time) {
        // console.log(mem[0]);
        const sql = `INSERT INTO logs_cpu(minion_id,idle,cpu_used,create_time) VALUES('${user_id}',${idle},${persent_cpu_used},'${create_time}')`
        const result = await this.client.query(sql);
        return result
    }
}